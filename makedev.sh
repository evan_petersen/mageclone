#!/bin/sh

cd $(dirname "$0")


#error handler
function error_handler() {
	echo "Error occurred in script at line: ${1}."
	echo "Line exited with status: ${2}"
}

trap 'error_handler ${LINENO} $?' ERR

#variables used in this script
	livePath=""
	key=""
	prefix=""
	liveDBUsername=""
	liveDBPassword=""
	liveDBName=""
	liveURL=""
	devPath=""
	devURL=""
	devDBName=""
	devDBUsername=""
	devDBPassword=""
	devDBConnectionTested="0"


#console printing methods
failMSG () {
	echo -e>&2 "\e[31m$@\e[39m"
}

successMSG () {
	echo -e>&2 "\e[32m$@\e[39m"
}

#rage quit method

die () {
    echo >&2 "$@"
    exit 1
}


printQuota() {

	quota -gs

}

#method grabs the full path to Magento installation
#checks for a valid local.xml, connects to DB and grabs live site url web/unsecure/base_url
collectLivePath() {

	while true; do

		read -p "Enter the full path to your Magento installation: /home/username/website.tld/html/: " livePath

		if [[ ! -d "${livePath}" ]]; then
			failMSG "Sorry, that path isn't valid"
			continue
		fi

		if [[ ! -f "${livePath}app/etc/local.xml" ]]; then
			failMSG "Sorry, I couldn't find your app/etc/local.xml file"
			continue
		fi

		key="$(echo "cat /config/global/crypt/key/text()" | xmllint --nocdata --shell ${livePath}app/etc/local.xml | sed '1d;$d')"
       		prefix="$(echo "cat /config/global/resources/db/table_prefix/text()" | xmllint --nocdata --shell ${livePath}app/etc/local.xml | sed '1d;$d')"
        	liveDBUsername="$(echo "cat /config/global/resources/default_setup/connection/username/text()" | xmllint --nocdata --shell ${livePath}app/etc/local.xml | sed '1d;$d')"
        	liveDBPassword="$(echo "cat /config/global/resources/default_setup/connection/password/text()" | xmllint --nocdata --shell ${livePath}app/etc/local.xml | sed '1d;$d')"
       		liveDBName="$(echo "cat /config/global/resources/default_setup/connection/dbname/text()" | xmllint --nocdata --shell ${livePath}app/etc/local.xml | sed '1d;$d')"
		liveDBHost="$(echo "cat /config/global/resources/default_setup/connection/host/text()" | xmllint --nocdata --shell ${livePath}app/etc/local.xml | sed '1d;$d')"

		mysql -h localhost -u ${liveDBUsername} --password=${liveDBPassword} ${liveDBName} -e exit 2>/dev/null
		if [[ $? -ne 0 ]]; then
			failMSG "Error: Connection to live DB failed with credentials stored in ${livePath}app/etc/local.xml"
                        continue
	        else
	                successMSG "Connection to live DB Successful"
	        fi


        	liveURL="$(echo "SELECT value as liveurl FROM ${prefix}core_config_data WHERE path = 'web/unsecure/base_url'" | mysql --host=${liveDBHost} --password=${liveDBPassword} --database=${liveDBName} --user=${liveDBUsername})"
		successMSG "I found your live website at: ${liveURL}"

		break
	done

}


collectDevPath() {

        while true; do

                read -p "Enter the full path to your desired Dev installation: /home/username/website.tld/html/: " devPath

		#check that path starts with / and ends with / and is 3 or more characters
		if [[ ! ${devPath} == /* ]] || [[ ! ${devPath} == */ ]] || [[ ${#devPath} -lt 3 ]]; then
			failMSG "Malformed dev path - must begin and end with '/'"
			continue
		fi


		#check if directory exists
                if [[ ! -d "${devPath}" ]]; then
                        failMSG "Sorry, that path isn't valid"
			continue
                fi

		#check if directory is empty - if not, ask user if they wish to clear its contents
		if [[ "$(ls -A ${devPath})" ]]; then
			failMSG "Directory not empty - should I delete its contents? [y/N]"
			read yn
			case $yn in
				[Yy]*) rm -rf ${devPath}*; rm -rf ${devPath}.??*; successMSG "removed contents of ${devPath}"; break;;
				*) failMSG "did not remove contents of ${devPath}"; break;
			esac
		else
			successMSG "${devPath} exists and is empty"
		fi

		#to-do implement disk space check/quota check

		break
	done
}


collectDevURL() {

	while true; do

		read -p "Enter the full URL to your desired Dev installation: http://dev.domain.tld/: " devURL

		if [[ ! ${devURL} == http://* ]] || [[ ! ${devURL} == */ ]] || [[ ${#devURL} -lt 9 ]]; then
			failMSG "Error with dev url: must start with http:// and end with /"
			continue
		fi

		break
	done

}


collectDevDBName() {
	devDBConnectionTested=0
	while true; do

		read -p "Enter your dev database name: " devDBName

                if [[ ${#devDBName} -lt 1 ]]; then
                        failMSG "Error with dev database name: must be at least 1 character"
                        continue
                fi

		break
	done

}


collectDevDBUsername() {
	devDBConnectionTested=0
	while true; do
		read -p "Enter your dev database username: " devDBUsername

		if [[ ${#devDBUsername} -lt 1 ]]; then
			failMSG "Error with dev database username: must be at least 1 character"
			continue
		fi

		break
	done

}

collectDevDBPassword() {
	devDBConnectionTested=0
        while true; do
                read -p "Enter your dev database password: " devDBPassword

                if [[ ${#devDBPassword} -lt 1 ]]; then
                        failMSG "Error with dev database username: must be at least 1 character"
                        continue
                fi

                break
        done

}

checkDevDBConnection() {

	devDBConnectionTested="0"

	if [[ ${#devDBName} -lt 1 ]]; then
                failMSG "Dev DB Name not set"
                return 0
        fi

	if [[ ${#devDBUsername} -lt 1 ]]; then
		failMSG "Dev DB Username not set"
		return 0
	fi

	if [[ ${#devDBPassword} -lt 1 ]]; then
                failMSG "Dev DB Password not set"
                return 0
        fi

	mysql -h localhost -u ${devDBUsername} --password=${devDBPassword} ${devDBName} -e exit 2>/dev/null
        if [[ $? -ne 0 ]]; then
		failMSG "Connection to dev Database Failed"
                devDBConnectionTested="0"
	else 
		successMSG "Connection to dev Database Successful"
                devDBConnectionTested="1"
	fi

}

cloneLiveToDev() {

	if [[ ${devDBConnectionTested} != 1 ]]; then
		errorMSG "Dev DB Connection must be tested before you can clone"
		return 0
	fi

	if [[ ${#livePath} -lt 1 ]]; then
		errorMSG "live path must be set before you can clone files"
		return 0
	fi

	if [[ ${#devPath} -lt 1 ]]; then
		errorMSG "dev path must be set before you can clone files"
		return 0
	fi

	successMSG "copying live to dev"
	cp -a ${livePath}* ${devPath} #copy live site files to dev site
	cp local.xml ${devPath}app/etc/local.xml #copy local.xml for editing later
	cp .htaccess ${devPath} #copy clean .htaccess file

	successMSG "editing ${devPath}app/etc/local.xml with dev db credentials"
	#edit previously copied local.xml file - put in new DB credentials and port over old key
	sed -i "s/{{dbuser}}/${devDBUsername}/" ${devPath}app/etc/local.xml #edit config file
	sed -i "s/{{dbpassword}}/${devDBPassword}/" ${devPath}app/etc/local.xml #edit config file
	sed -i "s/{{dbname}}/${devDBName}/" ${devPath}app/etc/local.xml #edit config file
	sed -i "s/{{key}}/${key}/" ${devPath}app/etc/local.xml #edit config file
	sed -i "s/{{prefix}}/${prefix}/" ${devPath}app/etc/local.xml #edit config file

	#dump db, import db
	successMSG "dumping live database"
	mysqldump -u ${liveDBUsername} --password=${liveDBPassword} ${liveDBName} > dbexport.sql #exportDB
	successMSG "importing live database into dev database"
	mysql -h localhost --password=${devDBPassword} -D ${devDBName} --user=${devDBUsername} < dbexport.sql #importDB

	#edit dev magento db to have proper urls
	successMSG "preparing database operations for dev database"
	cp dbops.sql dbopsTEMP.sql
	sed -i "s,{3},${devURL}," dbopsTEMP.sql
	sed -i "s,{4},${devDBName},g" dbopsTEMP.sql
	sed -i "s,{prefix},${prefix},g" dbopsTEMP.sql

	#process db changes on dev
	successMSG "processing dev database changes"
	mysql -h localhost --password=${devDBPassword} -D ${devDBName} --user=${devDBUsername} < dbopsTEMP.sql

	successMSG "removing cache directory contents from dev"
	rm -rf ${devPath}var/cache/*

	successMSG "removing sessions directory contents from dev"
	rm -rf ${devPath}var/session/*

	successMSG "reindexing dev site"
	php ${devPath}shell/indexer.php reindexall

	successMSG "cleaning dev site logs"
	php ${devPath}shell/log.php clean

	successMSG "cleaning temporary files"
	rm dbopsTEMP.sql
	rm dbexport.sql

	successMSG "live site clone to dev complete - your dev site should be available at ${devURL}"


}

menu() {
	clear
	echo -e "OPTION\tNAME\t\tCURRENT DATA"
	echo ""
	echo -e "1\tLive Path\t\t${livePath}"
	echo -e "2\tDev Path\t\t${devPath}"
	echo -e "3\tDev URL\t\t${devURL}"
	echo -e "4\tDev DB Name\t\t${devDBName}"
	echo -e "5\tDev DB Username\t\t${devDBUsername}"
	echo -e "6\tDev DB Password\t\t${devDBPassword}"
	echo -e "7\tCheck DEV DB\t\t${devDBConnectionTested}"
	echo -e "CLONE\tClone live site to dev site"

}

printQuota
collectLivePath
collectDevPath
collectDevURL
collectDevDBName
collectDevDBUsername
collectDevDBPassword
checkDevDBConnection
cloneLiveToDev
#menu
