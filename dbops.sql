UPDATE  `{4}`.`{prefix}core_config_data` SET  `value` =  '{3}' WHERE `{prefix}core_config_data`.`path` ='web/unsecure/base_url';
UPDATE  `{4}`.`{prefix}core_config_data` SET  `value` =  '{{unsecure_base_url}}' WHERE  `{prefix}core_config_data`.`path` ='web/unsecure/base_link_url';
UPDATE  `{4}`.`{prefix}core_config_data` SET  `value` =  '{{unsecure_base_url}}skin/' WHERE  `{prefix}core_config_data`.`path` ='web/unsecure/base_skin_url';
UPDATE  `{4}`.`{prefix}core_config_data` SET  `value` =  '{{unsecure_base_url}}media/' WHERE  `{prefix}core_config_data`.`path` ='web/unsecure/base_media_url';
UPDATE  `{4}`.`{prefix}core_config_data` SET  `value` =  '{{unsecure_base_url}}js/' WHERE  `{prefix}core_config_data`.`path` ='web/unsecure/base_js_url';
UPDATE  `{4}`.`{prefix}core_config_data` SET  `value` =  '0' WHERE `{prefix}core_config_data`.`path` ='web/secure/use_in_frontend';
UPDATE  `{4}`.`{prefix}core_config_data` SET  `value` =  '0' WHERE `{prefix}core_config_data`.`path` ='web/secure/use_in_adminhtml';
